<?php

namespace Tests\Models;

use App\Models\Contracts\WalletBalance;
use App\Models\Exceptions\InvestorInvalidArgumentException;
use App\Models\Investor;
use PHPUnit\Framework\TestCase;

class InvestorTest extends TestCase
{
    private $defaultName = 'Investor Name';

    private function getInvestor(string $name = null): Investor
    {
        $name = $name ?? $this->defaultName;
        $walletBalanceMock = $this->createMock(WalletBalance::class);
        return new Investor($name, $walletBalanceMock);
    }

    public function testWalletBalance()
    {
        $investor = $this->getInvestor();

        $walletBalanceObject = $investor->walletBalance();

        $this->assertInstanceOf(WalletBalance::class, $walletBalanceObject);
    }

    public function testPositiveName()
    {
        $investor = $this->getInvestor($this->defaultName);

        $actualName = $investor->name();

        $this->assertEquals($this->defaultName, $actualName);
    }

    public function testNegativeName()
    {
        $this->expectException(InvestorInvalidArgumentException::class);

        $this->getInvestor('');
    }

}
