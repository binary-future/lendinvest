<?php

namespace Tests\Models;

use App\Models\Exceptions\WalletBalanceInvalidArgumentException;
use App\Models\WalletBalance;
use PHPUnit\Framework\TestCase;

class WalletBalanceTest extends TestCase
{
    private function getWalletBalance(float $balance = 0): WalletBalance
    {
        return new WalletBalance($balance);
    }

    public function testInitialWalletBalance()
    {
        $balance = new WalletBalance();

        $actualBalance = $balance->currentBalance();

        $this->assertEquals(0, $actualBalance);
    }

    /**
     * @dataProvider positiveIncreaseWalletDataProvider
     * @param float $initialBalance
     * @param float $value
     * @param float $expectedBalance
     */
    public function testPositiveIncreaseWalletBalance(
        float $initialBalance,
        float $value,
        float $expectedBalance
    )
    {
        $balance = $this->getWalletBalance($initialBalance);

        $actualBalance = $balance
            ->increase($value)
            ->currentBalance();

        $this->assertEquals($expectedBalance, $actualBalance);
    }

    public function positiveIncreaseWalletDataProvider()
    {
        return [
            [0, 15, 15],
            [0, 0.36, 0.36],
            [1, 1, 2],
            [0.64, 1.36, 2],
        ];
    }

    /**
     * @dataProvider negativeIncreaseWalletDataProvider
     * @param float $value
     * @param string $expectedException
     */
    public function testNegativeIncreaseWalletBalance(
        float $value,
        string $expectedException
    )
    {
        $this->expectException($expectedException);

        $balance = $this->getWalletBalance();

        $balance->increase($value);
    }

    public function negativeIncreaseWalletDataProvider()
    {
        return [
            [-1, WalletBalanceInvalidArgumentException::class],
            [0, WalletBalanceInvalidArgumentException::class],
        ];
    }

    /**
     * @dataProvider positiveDecreaseWalletDataProvider
     * @param float $initialBalance
     * @param float $value
     * @param float $expectedBalance
     */
    public function testPositiveDecreaseWalletBalance(
        float $initialBalance,
        float $value,
        float $expectedBalance
    )
    {
        $balance = $this->getWalletBalance($initialBalance);

        $actualBalance = $balance
            ->decrease($value)
            ->currentBalance();

        $this->assertEquals($expectedBalance, $actualBalance);
    }

    public function positiveDecreaseWalletDataProvider()
    {
        return [
            [100, 100, 0],
            [100.56, 0.36, 100.20],
        ];
    }

    /**
     * @dataProvider negativeDecreaseWalletDataProvider
     * @param float $initialBalance
     * @param float $value
     * @param string $expectedException
     */
    public function testNegativeDecreaseWalletBalance(
        float $initialBalance,
        float $value,
        string $expectedException
    )
    {
        $this->expectException($expectedException);

        $balance = $this->getWalletBalance($initialBalance);

        $balance->decrease($value);
    }

    public function negativeDecreaseWalletDataProvider()
    {
        return [
            [0, -1, WalletBalanceInvalidArgumentException::class],
            [0, 0, WalletBalanceInvalidArgumentException::class],
            [-1, 1, WalletBalanceInvalidArgumentException::class],
            [1, 0.999999, WalletBalanceInvalidArgumentException::class],
        ];
    }
}
