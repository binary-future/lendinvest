<?php

namespace Tests\Models;

use App\Models\Contracts\Investor;
use App\Models\Contracts\WalletBalance;
use App\Models\Exceptions\InvestmentInvalidArgumentException;
use App\Models\Exceptions\InvestmentWalletBalanceException;
use App\Models\Investment;
use PHPUnit\Framework\TestCase;

class InvestmentTest extends TestCase
{
    private $defaultAmount = 50;

    /**
     * @return \DateTime
     * @throws \Exception
     */
    private function getDefaultStartDate()
    {
        return new \DateTime('2019-07-10');
    }

    private function investorMock($defaultWalletBalance = 100)
    {
        $walletBalanceMock = $this->createMock(WalletBalance::class);
        $walletBalanceMock->method('currentBalance')
            ->willReturn($defaultWalletBalance);
        $investorMock = $this->createMock(Investor::class);
        $investorMock->method('walletBalance')
            ->willReturn($walletBalanceMock);
        return $investorMock;
    }

    private function createInvestment(array $args = [])
    {
        $investor = $args['investor'] ?? $this->investorMock();
        $amount = $args['amount'] ?? $this->defaultAmount;
        $startDate = $args['startDate'] ?? $this->getDefaultStartDate();

        return new Investment(
            $investor,
            $amount,
            $startDate
        );
    }

    /**
     * @throws InvestmentInvalidArgumentException
     */
    public function testPositiveInvestor()
    {
        $investment = $this->createInvestment();

        $investorObject = $investment->investor();

        $this->assertInstanceOf(Investor::class, $investorObject);
    }

    public function testNegativeInvestor()
    {
        $this->expectException(\TypeError::class);

        $this->createInvestment([
            'investor' => -1
        ]);
    }

    /**
     * @throws InvestmentInvalidArgumentException
     * @throws \Exception
     */
    public function testStartDate()
    {
        $startDate = new \DateTime('2019-07-10');
        $investment = $this->createInvestment([
            'startDate' => $startDate
        ]);

        $actualStartDate = $investment->startDate();

        $this->assertSame($startDate, $actualStartDate);
    }

    /**
     * @dataProvider positiveAmountDataProvider
     * @param float $amount
     * @throws InvestmentInvalidArgumentException
     */
    public function testPositiveAmount(float $amount)
    {
        $investment = $this->createInvestment([
            'amount' => $amount
        ]);

        $actualAmount = $investment->amount();

        $this->assertEquals($amount, $actualAmount);
    }

    public function positiveAmountDataProvider()
    {
        return [
            [0.01], [10.99], [100]
        ];
    }

    /**
     * @dataProvider negativeAmountDataProvider
     * @param float $amount
     * @throws InvestmentInvalidArgumentException
     */
    public function testNegativeAmount(float $amount)
    {
        $this->expectException(InvestmentInvalidArgumentException::class);

        $investment = $this->createInvestment([
            'amount' => $amount
        ]);

        $actualAmount = $investment->amount();

        $this->assertEquals($amount, $actualAmount);
    }

    public function negativeAmountDataProvider()
    {
        return [
            [-0.01], [0]
        ];
    }

    /**
     * @dataProvider amountNOTMoreThanInvestorHasDataProvider
     * @param float $walletBalance
     * @param float $investmentAmount
     * @throws InvestmentInvalidArgumentException
     */
    public function testAmountNOTMoreThanInvestorHas(
        float $walletBalance,
        float $investmentAmount
    )
    {
        $investment = $this->createInvestment([
            'investor' => $this->investorMock($walletBalance),
            'amount' => $investmentAmount
        ]);

        $actualAmount = $investment->amount();

        $this->assertEquals($investmentAmount, $actualAmount);
    }

    public function amountNOTMoreThanInvestorHasDataProvider()
    {
        return [
            [1, 0.5], [1, 1]
        ];
    }

    /**
     * @dataProvider amountMOREThanInvestorHasDataProvider
     * @param float $walletBalance
     * @param float $investmentAmount
     * @throws InvestmentInvalidArgumentException
     */
    public function testAmountMOREThanInvestorHas(
        float $walletBalance,
        float $investmentAmount
    )
    {
        $this->expectException(InvestmentWalletBalanceException::class);

        $this->createInvestment([
            'investor' => $this->investorMock($walletBalance),
            'amount' => $investmentAmount
        ]);
    }

    public function amountMOREThanInvestorHasDataProvider()
    {
        return [
            [1, 1.5], [0, 0.1]
        ];
    }

}
