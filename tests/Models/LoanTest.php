<?php

namespace Tests\Models;

use App\Models\Contracts\Investor;
use App\Models\Contracts\Tranche;
use App\Models\Contracts\WalletBalance;
use App\Models\Exceptions\InvestmentInvalidArgumentException;
use App\Models\Exceptions\InvestmentWalletBalanceException;
use App\Models\Exceptions\LoanInvalidArgumentException;
use App\Models\Investment;
use App\Models\Loan;
use PHPUnit\Framework\TestCase;

class LoanTest extends TestCase
{
    private function createLoan(string $startDate, string $endDate): Loan
    {
        $startDate = new \DateTime($startDate);
        $endDate = new \DateTime($endDate);
        return new Loan($startDate, $endDate);
    }

    /**
     * @dataProvider positiveDatesDataProvider
     * @param string $startDate
     * @param string $endDate
     * @throws \Exception
     */
    public function testPositiveDates(
        string $startDate,
        string $endDate
    )
    {
        $startDate = new \DateTime($startDate);
        $endDate = new \DateTime($endDate);
        $loan = new Loan($startDate, $endDate);

        $actualStartDate = $loan->startDate();
        $actualEndDate = $loan->endDate();

        $this->assertSame($startDate, $actualStartDate);
        $this->assertSame($endDate, $actualEndDate);
    }

    public function positiveDatesDataProvider()
    {
        return [
            ['2019-07-10', '2019-07-11'],
            ['2019-07-10', '2020-07-10']
        ];
    }

    /**
     * @dataProvider negativeDatesDataProvider
     * @param string $startDate
     * @param string $endDate
     * @throws \Exception
     */
    public function testNegativeDates(
        string $startDate,
        string $endDate
    )
    {
        $this->expectException(LoanInvalidArgumentException::class);

        $this->createLoan($startDate, $endDate);
    }

    public function negativeDatesDataProvider()
    {
        return [
            ['2019-07-10', '2019-07-09'],
            ['2019-07-10', '2019-07-10']
        ];
    }

    public function testAddTranches()
    {
        $loan = $this->createLoan('2019-07-10', '2019-07-11');
        $tranche1Mock = $this->createMock(Tranche::class);
        $tranche2Mock = $this->createMock(Tranche::class);

        $loan->addTranche($tranche1Mock)
            ->addTranche($tranche2Mock);

        $this->assertCount(2, $loan->tranches());
    }

}
