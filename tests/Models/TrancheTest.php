<?php

namespace Tests\Models;

use App\Models\Contracts\Investment as InvestmentInterface;
use App\Models\Contracts\Investor;
use App\Models\Contracts\WalletBalance;
use App\Models\Exceptions\TrancheInvalidArgumentException;
use App\Models\Exceptions\TrancheInvalidInvestmentException;
use App\Models\Investment;
use App\Models\Tranche;
use PHPUnit\Framework\TestCase;

class TrancheTest extends TestCase
{
    private function createTranche(array $args = [])
    {
        $maxAmount = $args['maxAmount'] ?? 100;
        $interestRate = $args['interestRate'] ?? 0.3;

        return new Tranche(
            $maxAmount,
            $interestRate
        );
    }

    private function createInvestmentMock(
        float $investmentAmount,
        string $investmentStartDate,
        float $expectedEarn
    )
    {
        $walletMock = $this->createMock(WalletBalance::class);
        $walletMock->expects($this->once())
            ->method('increase')
            ->with($expectedEarn);
        $investorMock = $this->createMock(Investor::class);
        $investorMock->expects($this->once())
            ->method('walletBalance')
            ->willReturn($walletMock);
        $investorMock->expects($this->once())
            ->method('name')
            ->willReturn('Dummy investor');
        $investmentMock = $this->createMock(InvestmentInterface::class);
        $investmentMock->expects($this->atLeast(2))
            ->method('amount')
            ->willReturn($investmentAmount);
        $investmentMock->expects($this->once())
            ->method('startDate')
            ->willReturn(new \DateTime($investmentStartDate));
        $investmentMock->expects($this->once())
            ->method('investor')
            ->willReturn($investorMock);
        return $investmentMock;
    }

    /**
     * @throws TrancheInvalidArgumentException
     */
    public function testCreateEmptyTranche()
    {
        $this->expectException(\TypeError::class);

        new Tranche();
    }

    /**
     * @dataProvider positiveMaxAmountDataProvider
     * @param float $maxAmount
     * @throws TrancheInvalidArgumentException
     */
    public function testPositiveMaxAmount(float $maxAmount)
    {
        $tranche = $this->createTranche([
            'maxAmount' => $maxAmount
        ]);

        $actualMaxAmount = $tranche->maxAmount();

        $this->assertEquals($maxAmount, $actualMaxAmount);
    }

    public function positiveMaxAmountDataProvider()
    {
        return [
            [0.1], [1], [999999999]
        ];
    }

    /**
     * @dataProvider negativeMaxAmountDataProvider
     * @param float $maxAmount
     * @throws TrancheInvalidArgumentException
     */
    public function testNegativeMaxAmount(float $maxAmount)
    {
        $this->expectException(TrancheInvalidArgumentException::class);

        $this->createTranche([
            'maxAmount' => $maxAmount
        ]);
    }

    public function negativeMaxAmountDataProvider()
    {
        return [
            [-0.1], [0]
        ];
    }

    /**
     * @dataProvider positiveAddOneInvestmentDataProvider
     * @param float $maxAmount
     * @param float $investmentAmount
     * @throws TrancheInvalidArgumentException
     * @throws TrancheInvalidInvestmentException
     */
    public function testPositiveAddOneInvestment(
        float $maxAmount,
        float $investmentAmount
    )
    {
        $tranche = $this->createTranche([
            'maxAmount' => $maxAmount
        ]);
        $investmentMock = $this->createMock(Investment::class);
        $investmentMock->expects($this->once())
            ->method('amount')
            ->willReturn($investmentAmount);

        $investments = $tranche
            ->addInvestment($investmentMock)
            ->investments();

        $this->assertCount(1, $investments);
    }

    public function positiveAddOneInvestmentDataProvider()
    {
        return [
            [100, 100], [100, 10.50]
        ];
    }

    /**
     * @dataProvider negativeAddOneInvestmentDataProvider
     * @param float $maxAmount
     * @param float $investmentAmount
     * @throws TrancheInvalidArgumentException
     * @throws TrancheInvalidInvestmentException
     */
    public function testNegativeAddOneInvestment(
        float $maxAmount,
        float $investmentAmount
    )
    {
        $this->expectException(TrancheInvalidInvestmentException::class);

        $tranche = $this->createTranche([
            'maxAmount' => $maxAmount
        ]);
        $investmentMock = $this->createMock(Investment::class);
        $investmentMock->expects($this->once())
            ->method('amount')
            ->willReturn($investmentAmount);

        $tranche->addInvestment($investmentMock);
    }

    public function negativeAddOneInvestmentDataProvider()
    {
        return [
            [100, 150], [100, 100.1]
        ];
    }

    /**
     * @dataProvider positiveAddSeveralInvestmentsDataProvider
     * @param float $maxAmount
     * @param array $investmentAmounts
     * @throws TrancheInvalidArgumentException
     * @throws TrancheInvalidInvestmentException
     */
    public function testPositiveAddSeveralInvestments(
        float $maxAmount,
        array $investmentAmounts
    )
    {
        $tranche = $this->createTranche([
            'maxAmount' => $maxAmount
        ]);

        foreach ($investmentAmounts as $investmentAmount) {
            $investmentMock = $this->createMock(Investment::class);
            $investmentMock->expects($this->atLeastOnce())
                ->method('amount')
                ->willReturn($investmentAmount);

            $tranche->addInvestment($investmentMock);
        }

        $investmentAmounts = $tranche->investments();

        $this->assertCount(count($investmentAmounts), $investmentAmounts);
    }

    public function positiveAddSeveralInvestmentsDataProvider()
    {
        return [
            [100, [50, 50]], [100, [10.50, 15, 70]]
        ];
    }

    /**
     * @dataProvider negativeAddSeveralInvestmentDataProvider
     * @param float $maxAmount
     * @param array $investmentAmounts
     * @throws TrancheInvalidArgumentException
     * @throws TrancheInvalidInvestmentException
     */
    public function testNegativeAddSeveralInvestment(
        float $maxAmount,
        array $investmentAmounts
    )
    {
        $this->expectException(TrancheInvalidInvestmentException::class);

        $tranche = $this->createTranche([
            'maxAmount' => $maxAmount
        ]);

        foreach ($investmentAmounts as $investmentAmount) {
            $investmentMock = $this->createMock(Investment::class);
            $investmentMock->expects($this->atLeastOnce())
                ->method('amount')
                ->willReturn($investmentAmount);

            $tranche->addInvestment($investmentMock);
        }
    }

    public function negativeAddSeveralInvestmentDataProvider()
    {
        return [
            [100, [50, 50.01]], [100, [0.1, 100.1]]
        ];
    }

    /**
     * @dataProvider positiveInterestRateDataProvider
     * @param float $interestedRate
     * @throws TrancheInvalidArgumentException
     */
    public function testPositiveInterestRate(float $interestedRate)
    {
        $tranche = $this->createTranche([
            'interestRate' => $interestedRate
        ]);

        $actualRate = $tranche->interestRate();

        $this->assertEquals($interestedRate, $actualRate);
    }

    public function positiveInterestRateDataProvider()
    {
        return [
            [0.01], [0.1], [0.99]
        ];
    }

    /**
     * @dataProvider negativeInterestRateDataProvider
     * @param float $interestedRate
     * @throws TrancheInvalidArgumentException
     */
    public function testNegativeInterestRate(float $interestedRate)
    {
        $this->expectException(TrancheInvalidArgumentException::class);

        $this->createTranche([
            'interestRate' => $interestedRate
        ]);
    }

    public function negativeInterestRateDataProvider()
    {
        return [
            [0], [-0.1], [1.00], [1.01]
        ];
    }

    /**
     * @dataProvider positiveCalculateEarnsDataProvider
     * @param \DateTime $loanStartDate
     * @param \DateTime $yearMonth
     * @param array $investments
     * @param float $interestedRate
     * @param float $maxAmount
     * @throws TrancheInvalidInvestmentException
     * @throws \App\Models\Exceptions\WalletBalanceInvalidArgumentException
     */
    public function testPositiveCalculateEarns(
        \DateTime $loanStartDate,
        \DateTime $yearMonth,
        array $investments,
        float $interestedRate,
        float $maxAmount
    )
    {
        $this->expectNotToPerformAssertions();

        $tranche = $this->createTranche([
            'maxAmount' => $maxAmount,
            'interestRate' => $interestedRate
        ]);

        foreach ($investments as $investment) {

            $investmentMock = $this->createInvestmentMock(
                $investment['invest'],
                $investment['date'],
                $investment['earn']
            );

            $tranche->addInvestment($investmentMock);
        }

        $tranche->calculateEarns($loanStartDate, $yearMonth);
    }

    public function positiveCalculateEarnsDataProvider()
    {
        return [
            [
                new \DateTime('2019-07-01'),
                new \DateTime('2019-07'),
                [
                    [
                        'invest' => 50,
                        'date' => '2019-07-03',
                        'earn' => 14.03 // 50 / 31 days * 29 days * 0.3
                    ],
                    [
                        'invest' => 50,
                        'date' => '2019-06-10',
                        'earn' => 15 // 50 / 31 days * 31 days * 0.3
                    ]
                ],
                0.3,
                100
            ]
        ];
    }

}
