<?php
declare(strict_types = 1);

namespace Tests\Utils\Container;

use App\Utils\Container\Container;
use App\Utils\Container\Exceptions\ContainerException;
use PHPUnit\Framework\TestCase;
use Tests\Utils\Container\Fakes\FakeClass;
use Tests\Utils\Container\Fakes\FakeClassInterface;
use Tests\Utils\Container\Fakes\FakeAnotherClass;
use Tests\Utils\Container\Fakes\FakeNestedDependencyClass;
use Tests\Utils\Container\Fakes\NestedDependencyInterface;

class ContainerTest extends TestCase
{
    private $fakeInterface = FakeClassInterface::class;
    private $fakeClass = FakeClass::class;
    private $fakeAnotherClass = FakeAnotherClass::class;
    private $fakeNestedDependencyClass = FakeNestedDependencyClass::class;

    private function getContainer(): Container
    {
        return new Container();
    }

    /**
     * @dataProvider bindDataProvider
     * @param string $interface
     * @param string $expectedClass
     * @throws \App\Utils\Container\Exceptions\ContainerException
     * @throws \ReflectionException
     */
    public function testPositiveBindInterface(
        string $interface,
        ?string $dependency,
        string $expectedClass
    )
    {
        $container = $this->getContainer();

        $container->set($interface, $dependency);
        $actualClass = $container->get($interface);

        $this->assertInstanceOf($expectedClass, $actualClass);
    }

    public function bindDataProvider(): array
    {
        return [
            [$this->fakeInterface, $this->fakeClass, $this->fakeClass],
            [$this->fakeInterface, $this->fakeAnotherClass, $this->fakeAnotherClass],
            [$this->fakeClass, null, $this->fakeClass],
        ];
    }

    /**
     * @dataProvider bindNestedDependencyDataProvider
     * @param string $interface
     * @param string $dependentClass
     * @param string $expectedClass
     * @throws \App\Utils\Container\Exceptions\ContainerException
     * @throws \ReflectionException
     */
    public function testPositiveBindNestedDependencyInterface(
        string $interface,
        string $dependentClass,
        string $expectedClass
    )
    {
        $container = $this->getContainer();
        $container->set($interface, $dependentClass);
        /** @var NestedDependencyInterface $firstLevelClass */
        $firstLevelClass = $container->get($interface);

        $nestedClass = $firstLevelClass->getDependency();

        $this->assertInstanceOf($expectedClass, $nestedClass);
    }

    public function bindNestedDependencyDataProvider(): array
    {
        return [
            [$this->fakeInterface, $this->fakeNestedDependencyClass, $this->fakeClass],
        ];
    }

    /**
     * @param string $interface
     * @param string $class
     * @throws \App\Utils\Container\Exceptions\ContainerException
     * @throws \ReflectionException
     */
    public function testNegativeBindInterface()
    {
        $this->expectException(ContainerException::class);
        $container = $this->getContainer();

        $container->get($this->fakeInterface);
    }

}
