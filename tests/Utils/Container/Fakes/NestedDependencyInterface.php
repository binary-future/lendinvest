<?php


namespace Tests\Utils\Container\Fakes;


interface NestedDependencyInterface
{
    public function getDependency();
}