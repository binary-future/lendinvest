<?php


namespace Tests\Utils\Container\Fakes;


class FakeNestedDependencyClass
    implements FakeClassInterface, NestedDependencyInterface
{
    /**
     * @var FakeClass
     */
    private $fakeClass;

    public function __construct(FakeClass $fakeClass)
    {
        $this->fakeClass = $fakeClass;
    }

    /**
     * @return FakeClass
     */
    public function getDependency()
    {
        return $this->fakeClass;
    }


}