<?php


namespace App\Scenarios;


use App\Models\Investment;
use App\Models\Investor;
use App\Models\Loan;
use App\Models\Tranche;
use App\Models\WalletBalance;

final class Scenario1
{
    public function execute()
    {
        //TODO: create factories
        $loan = $this->createLoan('01-10-2015', '15-11-2015');
        $trancheA = $this->createTranche(0.03);
        $trancheB = $this->createTranche(0.06);

        $loan->addTranche($trancheA)
            ->addTranche($trancheB);

        $investor1 = $this->createInvestor('Investor 1');
        $investor2 = $this->createInvestor('Investor 2');
        $investor3 = $this->createInvestor('Investor 3');
        $investor4 = $this->createInvestor('Investor 4');


        $this->printStep($investor1->name(), 1000, 'A', '03-10-2015');
        $investment1 = $this->createInvestment($investor1, 1000, '03-10-2015');
        $trancheA->addInvestment($investment1);

        $this->printStep($investor2->name(), 1, 'A', '04-10-2015');
        try {
            $investment2 = $this->createInvestment($investor2, 1, '04-10-2015');
            $trancheA->addInvestment($investment2);
        } catch (\Throwable $ex) {
            echo $ex->getMessage() . PHP_EOL;
        }

        $this->printStep($investor3->name(), 500, 'B', '10-10-2015');
        $investment3 = $this->createInvestment($investor3, 500, '10-10-2015');
        $trancheB->addInvestment($investment3);

        $this->printStep($investor4->name(), 1100, 'B', '25-10-2015');
        try {
            $investment4 = $this->createInvestment($investor4, 1100, '25-10-2015');
            $trancheB->addInvestment($investment4);
        } catch (\Throwable $ex) {
            echo $ex->getMessage();
        }

        echo PHP_EOL;
        $loan->calculateEarns(new \DateTime('2015-10'));
    }

    private function createLoan(string $startDate, string $endDate)
    {
        return new Loan(
            new \DateTime($startDate),
            new \DateTime($endDate)
        );
    }

    private function createTranche(float $interestRate)
    {
        return new Tranche(
            1000,
            $interestRate
        );
    }

    private function createInvestor(string $name)
    {
        $wallet = new WalletBalance(1000);
        return new Investor(
            $name,
            $wallet
        );
    }

    private function createInvestment(
        Investor $investor,
        float $amount,
        string $startDate
    )
    {
        return new Investment(
            $investor,
            $amount,
            new \DateTime($startDate)
        );
    }

    private function printStep(
        string $investorName,
        float $amount,
        string $trancheName,
        string $date
    )
    {
        echo "As “{$investorName}” I’d like to invest $amount pounds on the tranche “{$trancheName}” on $date" . PHP_EOL;
    }
}