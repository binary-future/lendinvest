<?php


namespace App\Models;


use App\Models\Contracts\WalletBalance as WalletBalanceInterface;
use App\Models\Exceptions\WalletBalanceInvalidArgumentException;

class WalletBalance implements WalletBalanceInterface
{
    protected $balance;

    /**
     * WalletBalance constructor.
     * @param float $balance
     * @throws WalletBalanceInvalidArgumentException
     */
    public function __construct(float $balance = 0)
    {
        $this->validateBalanceValue($balance, true);

        $this->balance = $balance;
    }

    /**
     * @return float
     */
    public function currentBalance(): float
    {
        return $this->balance;
    }

    /**
     * @param float $value
     * @return $this
     * @throws WalletBalanceInvalidArgumentException
     */
    public function increase(float $value): WalletBalanceInterface
    {
        $this->balance += $this->prepareBalanceValue($value);
        return $this;
    }

    /**
     * @param float $value
     * @return $this
     * @throws WalletBalanceInvalidArgumentException
     */
    public function decrease(float $value): WalletBalanceInterface
    {
        $this->balance -= $this->prepareBalanceValue($value);
        return $this;
    }

    /**
     * @param float $value
     * @return float
     * @throws WalletBalanceInvalidArgumentException
     */
    protected function prepareBalanceValue(float $value)
    {
        $this->validateBalanceValue($value);

        $roundedValue = $this->roundToTwoDecimals($value);

        if ($roundedValue !== $value) {
            throw WalletBalanceInvalidArgumentException
                ::balanceValueShouldBeRoundedToTwoDecimalsBeforeUse($value);
        }

        return $value;
    }

    /**
     * @param float $value
     * @param bool $isDefault
     * @throws WalletBalanceInvalidArgumentException
     */
    protected function validateBalanceValue(float $value, bool $isDefault = false)
    {
        if ($isDefault && $value < 0) {
            throw WalletBalanceInvalidArgumentException::balanceDefaultValueShouldBePositiveOrZero();
        }

        if (! $isDefault && $value <= 0) {
            throw WalletBalanceInvalidArgumentException::balanceValueShouldBePositive();
        }
    }

    /**
     * @param float $value
     * @return float
     */
    protected function roundToTwoDecimals(float $value): float
    {
        // because of round($value, 2, PHP_ROUND_HALF_DOWN) is not work
        return floor($value * 100) / 100;
    }
}