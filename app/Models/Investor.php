<?php


namespace App\Models;


use App\Models\Contracts\Investor as InvestorInterface;
use App\Models\Contracts\WalletBalance as WalletBalanceInterface;
use App\Models\Exceptions\InvestorInvalidArgumentException;

class Investor implements InvestorInterface
{
    /**
     * @var WalletBalanceInterface
     */
    protected $walletBalance;
    /**
     * @var string
     */
    private $name;

    /**
     * Investor constructor.
     * @param string $name
     * @param WalletBalanceInterface $walletBalance
     * @throws InvestorInvalidArgumentException
     */
    public function __construct(string $name, WalletBalanceInterface $walletBalance)
    {
        $this->validateName($name);

        $this->walletBalance = $walletBalance;
        $this->name = $name;
    }

    /**
     * @return WalletBalanceInterface
     */
    public function walletBalance(): WalletBalanceInterface
    {
        return $this->walletBalance;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @throws InvestorInvalidArgumentException
     */
    protected function validateName(string $name)
    {
        if (strlen($name) === 0) {
            throw InvestorInvalidArgumentException::nameShouldNotBeEmpty();
        }
    }
}