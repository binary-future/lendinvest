<?php


namespace App\Models;


use App\Models\Contracts\Investment as InvestmentInterface;
use App\Models\Contracts\Investor as InvestorInterface;
use App\Models\Contracts\Tranche as TrancheInterface;
use App\Models\Exceptions\TrancheInvalidArgumentException;
use App\Models\Exceptions\TrancheInvalidInvestmentException;

class Tranche implements TrancheInterface
{
    /**
     * @var float
     */
    protected $maxAmount;
    /**
     * @var InvestmentInterface[]
     */
    protected $investments = [];
    /**
     * @var float
     */
    private $interestRate;

    /**
     * Investor constructor.
     * @param float $maxAmount
     * @param float $interestRate
     * @throws TrancheInvalidArgumentException
     */
    public function __construct(
        float $maxAmount,
        float $interestRate
    )
    {
        $this->validate($maxAmount, $interestRate);

        $this->maxAmount = $maxAmount;
        $this->interestRate = $interestRate;
    }

    /**
     * @param \DateTime $yearMonth
     * @throws Exceptions\WalletBalanceInvalidArgumentException
     */
    public function calculateEarns(\DateTime $loanStartDate, \DateTime $yearMonth)
    {
        $currentYear = $yearMonth->format('Y');
        $currentMonth = $yearMonth->format('n');
        $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $currentMonth, $currentYear);

        // set the latest day of month
        $yearMonth->setDate($currentYear, $currentMonth, $daysInMonth);

        foreach ($this->investments() as $investment) {
            $daysDiff = $investment->startDate()->diff($yearMonth)->days + 1;
            $currentMonthDays = $daysDiff > $daysInMonth ? $daysInMonth : $daysDiff;
            $earnDays = $currentMonthDays - $loanStartDate->format('j') + 1;

            $earn = $investment->amount() / $daysInMonth * $earnDays * $this->interestRate();
            // round to two decimals
            $earn = floor($earn * 100) / 100;

            $investor = $investment->investor();
            $investor->walletBalance()->increase($earn);
            // TODO: temporary log. Do some event when investor eans and listen it
            $this->earnLog($investor, $earn);
        }
    }

    /**
     * @return float
     */
    public function maxAmount(): float
    {
        return $this->maxAmount;
    }

    /**
     * @return float
     */
    public function interestRate(): float
    {
        return $this->interestRate;
    }

    /**
     * @param InvestmentInterface $investment
     * @return $this
     * @throws TrancheInvalidInvestmentException
     */
    public function addInvestment(InvestmentInterface $investment)
    {
        $this->validateInvestment($investment);

        $this->investments[] = $investment;
        return $this;
    }

    /**
     * @return InvestmentInterface[]
     */
    public function investments(): array
    {
        return $this->investments;
    }

    /**
     * @return float
     */
    protected function currentInvestmentsAmount(): float
    {
        $investmentsAmount = 0;

        foreach ($this->investments() as $investment) {
            $investmentsAmount += $investment->amount();
        }

        return $investmentsAmount;
    }

    /**
     * @param InvestmentInterface $investment
     * @throws TrancheInvalidInvestmentException
     */
    protected function validateInvestment(InvestmentInterface $investment)
    {
        $currentInvestmentsAmount = $this->currentInvestmentsAmount();
        $investmentAmount = $investment->amount();

        if ($this->maxAmount() < $currentInvestmentsAmount + $investmentAmount) {
            throw TrancheInvalidInvestmentException
                ::totalInvestmentAmountExceedsTrancheMaxAmount(
                    $currentInvestmentsAmount,
                    $investmentAmount,
                    $this->maxAmount()
                );
        }
    }

    /**
     * @param float $maxAmount
     * @param float $interestRate
     * @throws TrancheInvalidArgumentException
     */
    protected function validate(float $maxAmount, float $interestRate)
    {
        if ($maxAmount <= 0) {
            throw TrancheInvalidArgumentException
                ::maxAmountInvalidDiapason(0, $maxAmount);
        }

        if ($interestRate <= 0 || $interestRate >= 1) {
            throw TrancheInvalidArgumentException
                ::interestRateInvalidDiapason(0, 1, $interestRate);
        }
    }

    /**
     * @param InvestorInterface $investor
     * @param float $earn
     */
    protected function earnLog(InvestorInterface $investor, float $earn)
    {
        echo "{$investor->name()} earns $earn pounds" . PHP_EOL;
    }
}