<?php


namespace App\Models\Exceptions;


class InvestmentWalletBalanceException extends \Exception
{
    public static function exceedInvestorWalletBalance(
        float $amount,
        float $walletBalance
    ) : InvestmentWalletBalanceException
    {
        return new self(
            "Amount of investment ($amount) exceeds investor wallet balance ($walletBalance)"
        );
    }
}