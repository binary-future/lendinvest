<?php


namespace App\Models\Exceptions;


class TrancheInvalidInvestmentException extends \Exception
{
    public static function totalInvestmentAmountExceedsTrancheMaxAmount(
        float $currentInvestmentAmount,
        float $investmentAmount,
        float $trancheMaxAmount
    ) : TrancheInvalidInvestmentException
    {
        return new self(
            'Total investment amount exceeds ' .
            "tranche maximum amount ($trancheMaxAmount)." .
            "Current investment amount is $currentInvestmentAmount" .
            "Your investment amount is $investmentAmount"
        );
    }
}