<?php


namespace App\Models\Exceptions;


class WalletBalanceInvalidArgumentException extends \Exception
{
    public static function balanceValueShouldBePositive()
        : WalletBalanceInvalidArgumentException
    {
        return new self('Wallet balance can be changed only on positive value');
    }

    public static function balanceDefaultValueShouldBePositiveOrZero()
        : WalletBalanceInvalidArgumentException
    {
        return new self('Wallet balance default value should be positive value or zero');
    }

    public static function balanceValueShouldBeRoundedToTwoDecimalsBeforeUse(float $value)
        : WalletBalanceInvalidArgumentException
    {
        return new self("Balance value should be rounded to two decimals before use. " .
            "Got $value");
    }
}