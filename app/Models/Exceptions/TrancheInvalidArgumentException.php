<?php


namespace App\Models\Exceptions;


class TrancheInvalidArgumentException extends \Exception
{
    public static function interestRateInvalidDiapason(
        float $greaterThan,
        float $lessThan,
        float $actualValue
    ) : TrancheInvalidArgumentException
    {
        return new self(
            "Interest rate must be greater than $greaterThan " .
            "and less than $lessThan. Got $actualValue"
        );
    }

    public static function maxAmountInvalidDiapason(
        float $greaterThan,
        float $actualValue
    ) : TrancheInvalidArgumentException
    {
        return new self(
            "Max amount must be greater than $greaterThan." .
            "Got $actualValue"
        );
    }
}