<?php


namespace App\Models\Exceptions;


class InvestorInvalidArgumentException extends \Exception
{
    public static function nameShouldNotBeEmpty()
        : InvestorInvalidArgumentException
    {
        return new self('Name should not be empty');
    }
}