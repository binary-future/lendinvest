<?php


namespace App\Models\Exceptions;


class LoanInvalidArgumentException extends \Exception
{
    public static function endDateShouldBeGreaterThanStartDate(
        \DateTime $startDate,
        \DateTime $endDate
    ) : LoanInvalidArgumentException
    {
        $format = 'MM/dd/yyyy';
        return new self(
            "End date ({$endDate->format($format)}) should be greater than start date ({$startDate->format($format)})"
        );
    }
}