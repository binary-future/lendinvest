<?php


namespace App\Models\Exceptions;


class InvestmentInvalidArgumentException extends \Exception
{
    public static function amountInvalidDiapason(
        float $greaterThan,
        float $actualValue
    ) : InvestmentInvalidArgumentException
    {
        return new self(
            "Amount must be greater than $greaterThan." .
            "Got $actualValue"
        );
    }
}