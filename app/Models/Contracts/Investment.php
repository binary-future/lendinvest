<?php

namespace App\Models\Contracts;

use App\Models\Contracts\Investor as InvestorInterface;

interface Investment
{
    /**
     * @return \DateTime
     */
    public function startDate(): \DateTime;

    /**
     * @return InvestorInterface
     */
    public function investor(): InvestorInterface;

    /**
     * @return float
     */
    public function amount(): float;
}