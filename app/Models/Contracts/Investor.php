<?php

namespace App\Models\Contracts;

use App\Models\Contracts\WalletBalance as WalletBalanceInterface;

interface Investor
{
    /**
     * @return string
     */
    public function name(): string;

    /**
     * @return WalletBalance
     */
    public function walletBalance(): WalletBalanceInterface;
}