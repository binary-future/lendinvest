<?php

namespace App\Models\Contracts;

use App\Models\Contracts\Investment as InvestmentInterface;
use App\Models\Exceptions\TrancheInvalidInvestmentException;
use App\Models\Exceptions\WalletBalanceInvalidArgumentException;

interface Tranche
{
    /**
     * @return float
     */
    public function maxAmount(): float;

    /**
     * @param InvestmentInterface $investment
     * @return \App\Models\Tranche
     * @throws TrancheInvalidInvestmentException
     */
    public function addInvestment(InvestmentInterface $investment);

    /**
     * @return InvestmentInterface[]
     */
    public function investments(): array;


    /**
     * @param \DateTime $yearMonth
     * @throws WalletBalanceInvalidArgumentException
     */
    public function calculateEarns(\DateTime $loanStartDate, \DateTime $yearMonth);
}