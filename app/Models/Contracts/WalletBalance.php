<?php

namespace App\Models\Contracts;

use App\Models\Exceptions\WalletBalanceInvalidArgumentException;

interface WalletBalance
{
    /**
     * @return float
     */
    public function currentBalance(): float;

    /**
     * @param float $value
     * @return \App\Models\WalletBalance
     * @throws WalletBalanceInvalidArgumentException
     */
    public function increase(float $value): WalletBalance;

    /**
     * @param float $value
     * @return \App\Models\WalletBalance
     * @throws WalletBalanceInvalidArgumentException
     */
    public function decrease(float $value): WalletBalance;
}