<?php


namespace App\Models;


use App\Models\Contracts\Investment as InvestmentInterface;
use App\Models\Contracts\Investor as InvestorInterface;
use App\Models\Exceptions\InvestmentInvalidArgumentException;
use App\Models\Exceptions\InvestmentWalletBalanceException;

class Investment implements InvestmentInterface
{
    /**
     * @var InvestorInterface
     */
    protected $investor;
    /**
     * @var float
     */
    private $amount;
    /**
     * @var \DateTime
     */
    private $startDate;

    /**
     * Investor constructor.
     * @param InvestorInterface $investor
     * @param float $amount
     * @param \DateTime $startDate
     * @throws InvestmentInvalidArgumentException
     * @throws InvestmentWalletBalanceException
     */
    public function __construct(
        InvestorInterface $investor,
        float $amount,
        \DateTime $startDate
    )
    {
        $this->validate($investor, $amount);

        $this->amount = $amount;

        $this->investor = $investor;
        $this->startDate = $startDate;
    }

    /**
     * @return \DateTime
     */
    public function startDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @return InvestorInterface
     */
    public function investor(): InvestorInterface
    {
        return $this->investor;
    }

    /**
     * @return float
     */
    public function amount(): float
    {
        return $this->amount;
    }

    /**
     * @param InvestorInterface $investor
     * @param float $amount
     * @throws InvestmentInvalidArgumentException
     * @throws InvestmentWalletBalanceException
     */
    protected function validate(InvestorInterface $investor, float $amount)
    {
        if ($amount <= 0) {
            throw InvestmentInvalidArgumentException
                ::amountInvalidDiapason(0, $amount);
        }

        if ($this->doesWalletBalanceExceed($investor, $amount)) {
            throw InvestmentWalletBalanceException
                ::exceedInvestorWalletBalance($amount, $investor->walletBalance()->currentBalance());
        }
    }

    /**
     * @param InvestorInterface $investor
     * @param float $amount
     * @return bool
     */
    protected function doesWalletBalanceExceed(InvestorInterface $investor, float $amount): bool
    {
        return $investor->walletBalance()->currentBalance() < $amount;
    }
}