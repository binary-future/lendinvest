<?php


namespace App\Models;


use App\Models\Contracts\Tranche as TrancheInterface;
use App\Models\Exceptions\LoanInvalidArgumentException;

class Loan
{
    /**
     * @var \DateTime
     */
    private $startDate;
    /**
     * @var \DateTime
     */
    private $endDate;
    /**
     * @var TrancheInterface[]
     */
    private $tranches;

    /**
     * Investor constructor.
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @throws LoanInvalidArgumentException
     */
    public function __construct(\DateTime $startDate, \DateTime $endDate)
    {
        $this->validateDates($startDate, $endDate);

        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @param TrancheInterface $tranche
     * @return Loan
     */
    public function addTranche(TrancheInterface $tranche)
    {
        $this->tranches[] = $tranche;
        return $this;
    }

    /**
     * @return TrancheInterface[]
     */
    public function tranches(): array
    {
        return $this->tranches;
    }

    /**
     * @param \DateTime $yearMonth
     * @throws Exceptions\WalletBalanceInvalidArgumentException
     */
    public function calculateEarns(\DateTime $yearMonth)
    {
        foreach ($this->tranches() as $tranch) {
            $tranch->calculateEarns($this->startDate, $yearMonth);
        }
    }

    /**
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @throws LoanInvalidArgumentException
     */
    private function validateDates(\DateTime $startDate, \DateTime $endDate)
    {
        if ($startDate >= $endDate) {
            throw LoanInvalidArgumentException
                ::endDateShouldBeGreaterThanStartDate($startDate, $endDate);
        }
    }

    /**
     * @return \DateTime
     */
    public function startDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @return \DateTime
     */
    public function endDate(): \DateTime
    {
        return $this->endDate;
    }
}