<?php
declare(strict_types = 1);

namespace App\Utils\Container;


use App\Utils\Container\Exceptions\ContainerException;

class Container
{
    /**
     * @var array
     */
    protected $instances = [];

    /**
     * @param string $abstract
     * @return object
     * @throws ContainerException
     * @throws \ReflectionException
     */
    public function get(string $abstract)
    {
        // if abstract is not registered yet
        if (! isset($this->instances[$abstract])) {
            $this->set($abstract);
        }

        return $this->resolve($this->instances[$abstract]);
    }

    /**
     * @param string $abstract
     * @param mixed $concrete
     * @return mixed
     */
    public function set(string $abstract, $concrete = null)
    {
        if ($concrete === null) {
            $concrete = $abstract;
        }

        return $this->instances[$abstract] = $concrete;
    }

    /**
     * @param $concrete
     * @return object
     * @throws ContainerException
     * @throws \ReflectionException
     */
    protected function resolve($concrete)
    {
        $reflector = new \ReflectionClass($concrete);

        // check if concrete class is instantiable
        if (! $reflector->isInstantiable()) {
            throw ContainerException::notInstantiableClass($concrete);
        }

        $constructor = $reflector->getConstructor();
        if ($constructor === null) {
            return $reflector->newInstance();
        }

        $parameters = $constructor->getParameters();
        $dependencies = $this->getDependencies($parameters);

        return $reflector->newInstanceArgs($dependencies);
    }

    /**
     * @param \ReflectionParameter[] $parameters
     * @return array
     * @throws ContainerException
     * @throws \ReflectionException
     */
    protected function getDependencies(array $parameters)
    {
        $dependencies = [];
        foreach ($parameters as $parameter) {
            // get class of parameter
            $dependency = $parameter->getClass();
            if ($dependency === null) {
                // check default value for parameter
                if ($parameter->isDefaultValueAvailable()) {
                    // set default value to dependencies
                    $dependencies[] = $parameter->getDefaultValue();
                } else {
                    throw ContainerException::cannotResolveDependency($parameter->getName());
                }
            } else {
                $dependencies[] = $this->get($dependency->getName());
            }
        }

        return $dependencies;
    }
}