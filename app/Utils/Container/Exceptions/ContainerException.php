<?php


namespace App\Utils\Container\Exceptions;


class ContainerException extends \Exception
{
    public static function cannotResolveDependency(string $dependency): ContainerException
    {
        return new self("Can not resolve class dependency $dependency");
    }

    public static function notInstantiableClass(string $dependency): ContainerException
    {
        return new self("Class {$dependency} is not instantiable");
    }
}